import java.util.*;
public class Kereta {
    private int jumlahtiket;
    private String namakereta;
    private List<Ticket> tikettiket;
    //Kereta Komuter
    public Kereta(){
        jumlahtiket=1000;
        this.tikettiket= new ArrayList<Ticket>();
    }
    //Kereta Api Jarak Jauh
    public Kereta(String namakereta,int jumlahtiket){
        this.namakereta=namakereta;
        this.jumlahtiket=jumlahtiket;
        this.tikettiket= new ArrayList<Ticket>();
    }
    //kereta komuter
    public void tambahTiket(String nama){
        if(jumlahtiket>0){
            jumlahtiket--;
            Ticket tiket = new Ticket();
            tiket.settiket(nama);
            tikettiket.add(tiket);
            System.out.println("=====================================================================");
            System.out.print("Tiket Berhasil Dipesan. ");
            if(jumlahtiket<30){
                System.out.println("Sisa tiket tersedia: "+jumlahtiket);
            }else{
                System.out.println();
            }           
        }else{
            System.out.println("=====================================================================");
            System.out.println("kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainya");
        }

        
    }
    //kereta KAJJ
    public void tambahTiket(String nama,String asal,String tujuan){
        //pengecekan tiket masih ada atau habis
        if(jumlahtiket>0){
            jumlahtiket--;
            Ticket tiket = new Ticket();
            tiket.settiket(nama,asal,tujuan);
            tikettiket.add(tiket);
            System.out.println("=====================================================================");
            System.out.print("Tiket Berhasil Dipesan. ");
            if(jumlahtiket<30){
                System.out.println("Sisa tiket tersedia: "+jumlahtiket);
            }else{
                System.out.println();
            }           
        }else{
            System.out.println("=====================================================================");
            System.out.println("kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainya");
        }
    }
    //method menampilkan tiket 
    public void tampilkanTiket(){
        System.out.println("=====================================================================");
        //pengecekan kereta KAJJ atau keerta komuter
        if(namakereta!=null){
            System.out.println("Daftar penumpang Kereta Api "+namakereta+":");
        }else{
            System.out.println("Daftar penumpang Kereta Api Komuter:");
        }
        int cetaksekali = 0;
        for (Ticket ticket : tikettiket) {
            if(namakereta!=null||cetaksekali==0){
                System.out.println("--------------------------");
                cetaksekali++;
            }
            System.out.println("Nama: "+ticket.getnama());
            if(ticket.getasal()!= null){
                System.out.println("Asal: "+ticket.getasal());
                System.out.println("Tujuan: "+ticket.gettujuan());
            }
        }
        System.out.println("--------------------------");
    }


    // Tulis kode disini
}